hostname=$1


counter=0
for arg in $*
do 
counter=$(expr $counter + 1) 
done


if ((counter<1))
then 
 echo "I think you forgot to add the I.P. address"
 exit 1
 elif ((counter>1))
 then
 echo "Too many arguments, just the IP please :)"
 exit 1
 else 
 echo "IP received, now to log into the machine."
fi 

ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ubuntu@$hostname << EOF

# First to install Node.js:

cd ~


# Install the PPA in order to get access to its packages
curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh



# Run the script gained from the above command
sudo bash nodesource_setup.sh


# Update packages
sudo apt update




# Install Node.js
sudo apt install nodejs -y



# Install modules and packages to use with Node.js
sudo apt install npm




# Now to install Paintjs:

# Download the zip folder
wget https://github.com/1j01/jspaint/archive/refs/heads/master.zip

sudo apt install unzip

echo "UNZIPPER INSTALLED"

# Unzip the folder
unzip master.zip


# Adress any issues with the code
(cd ~/jspaint-master; npm audit fix --force)





# Run Paintjs
#(cd ~/jspaint-master; npm run dev)

EOF


# sleep 5

# # Open PaintJs!
# open http://$hostname:1999



