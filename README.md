# Task

To write a script that installs JsPaint on a virtual machine. There are two script files which I have written: paint.sh and wrongpaint.sh. The latter contains my attempt to send text to a file in order to create an init script. From my understanding of init scripts, I wanted it to run JsPaint in the background while the bash script could open the browser page so that JsPaint could be used. The first file is the my submission for this task and requires a couple of manual steps at the end.


## Prerequisites

- You will need an ubuntu virtual machine. I chose Ubuntu Server 20.04 LTS (HVM) during my testing, t2.micro for instance type, default configuration and storage details. I added a tag with 'Name' in the Key section and something like '<Your name>-Paint' in the Value section. The security group needs to allow SSH from your own IP, HTTP and HTTPS from anywhere and it also needs to allow port 1999 from anywhere
- You will also need a keypair which will allow you to ssh into the virtual machine; I have been using Cohort9_shared
- The final thing you will need is paint.sh which is in this repo. You can access this by cloning this repo to your machine


## Running the script

This repo should be cloned to your machine so that you have access to the files. I would advise to carry out the command ```chmod +x paint.sh``` to ensure that it is executable. Next you can run the script using ```./paint.sh```.

Unfortunately I couldn't get the whole thing to run from the script alone. This means that, once the file has finished running, you will need to carry out a couple of things manually. First is to ssh into the machine. This can be done with the command:

```bash
ssh -i <path to your key>/<your key> ubuntu@<public IP of VM>
```
Once logged into the machine, run the command:
```bash
cd ~/jspaint-master; npm run dev
```

This will run JsPaint. Once this is done, you can enter the public IP of the VM into a new browser tab and JsPaint should be ready and waiting to be used! (If this doesn't work, paste the public IP into the browser and add :1999 to the end of it.)

